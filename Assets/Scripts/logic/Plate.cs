using System;
using UnityEngine;

namespace Chef
{
    public class Plate : MonoBehaviour
    {
        public Vegetable currentVegetable { get; private set; }
        public void Fill(Vegetable vegetable)
        {
            Debug.Log(vegetable.name);
            vegetable.transform.SetParent(transform);
            vegetable.transform.position = transform.position;
            currentVegetable = vegetable;
            // currentVegetable.transform.SetParent(transform);
            // TransitionHelper.MoveVegetable(currentVegetable.transform, transform.position);
        }

        public Vegetable Get()
        {
            return currentVegetable;
        }

        public void Clean()
        {
            currentVegetable = null;
        }

    }
}