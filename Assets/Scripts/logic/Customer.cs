using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

namespace Chef
{
    public class CustomerResult
    {
        public string customerID;
        public bool delivetStatus;
        public bool hasBonus;
    }
    public class CustometResultEvent : UnityEvent<string> { }
    public class Customer : MonoBehaviour
    {
        private string customerID;
        public Salad orderSalad { get; private set; }
        // A customer waits for a defined period before leaving dissatisfied.
        [SerializeField] ProgressBar waitingBar = null;
        private CustomerManager customerManager;
        private bool hasBonus = false;
        public static CustometResultEvent OnTimeOut = new CustometResultEvent();
        public void Init(Salad salad, CustomerManager customerManager)
        {
            orderSalad = salad;
            customerID = Guid.NewGuid().ToString();
            //The period depends on the amount of ingredients.
            float customerWaitTime = salad.vegetableCount * Settings.OrderWaitTimePerVegetable;
            waitingBar.Play(customerWaitTime);
            // after 30% of total time, bonus will be deactive
            DisableBonus(customerWaitTime * 30 / 100);
            waitingBar.OnComplete.AddListener(TimeRanOut);
            this.customerManager = customerManager;
        }

        private async void DisableBonus(float bonusDelay)
        {
            await Task.Delay(TimeSpan.FromSeconds(bonusDelay));
            hasBonus = false;
        }

        /// <summary>
        /// Delivering in time adds points to the player’s score count. If the time runs out, the customer will leave which will result in minus points for both players.
        /// </summary>
        private void TimeRanOut()
        {
            // customer received nothing
            if (OnTimeOut != null)
            {
                OnTimeOut.Invoke(customerID);
                Debug.Log("Invoking timeout");
            }
            else
            {
                Debug.Log("No timeout is listening");
            }
        }

        public CustomerResult ReceiveOrder(Salad salad)
        {
            bool match = true;
            // if vegetable counts in salad does not match with the order, return false
            if (salad.vegetableCount != orderSalad.vegetableCount)
            {
                match = false;
                Debug.Log("salad vegetable cound mismatch - " + salad.vegetableCount + " : " + orderSalad.vegetableCount);
            }
            else
            {
                // look for each vegetableType in salad in orderedSalad
                foreach (var veg in salad.vegetables)
                {
                    var vegetable = orderSalad.vegetables.Find(x => x.type == veg.type);
                    if (vegetable == null)
                    {
                        match = false;
                        Debug.Log("could not find " + veg.type + " in ordered salad");
                    }
                }
            }
            // if received salad is the same as ordered, increase score and ask customer manager to make a new customer
            if (match)
            {
                // ask customer manager to make a new customer
                Debug.Log("Correct");
                Destroy(gameObject);
                Destroy(salad.gameObject);
                customerManager.OrderSuccess(this);
            }
            else
            {
                // Angry customer: The waiting time for the customer decreases faster. 
                waitingBar.SetSpeed(Settings.AngryCustomerProgressSpeed);
                Debug.Log("Wrong order");
            }
            CustomerResult customerResult = new CustomerResult();
            customerResult.customerID = customerID;
            customerResult.delivetStatus = match;
            customerResult.hasBonus = hasBonus;
            return customerResult;
        }
    }
}