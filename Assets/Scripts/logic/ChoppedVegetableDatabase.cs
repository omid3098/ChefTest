using System.Collections.Generic;
using UnityEngine;

namespace Chef
{
    [CreateAssetMenu(menuName = "Chef/Chopped Vegetable DB")]
    public class ChoppedVegetableDatabase : ScriptableObject
    {
        public List<Vegetable> vegetables = new List<Vegetable>();
    }
}