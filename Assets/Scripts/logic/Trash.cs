using System;
using System.Threading.Tasks;
using DG.Tweening;
using UnityEngine;

namespace Chef
{
    public class Trash : MonoBehaviour
    {
        public async void Recycle(Salad salad)
        {
            salad.transform.SetParent(transform);
            TransitionHelper.MoveVegetable(salad.transform, transform.position);
            await Task.Delay(TimeSpan.FromSeconds(1f));
            var tween = salad.transform.DOScale(Vector3.zero, 0.5f);
            tween.onComplete += () =>
            {
                Destroy(salad.gameObject);
            };
        }
    }
}