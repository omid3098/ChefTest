﻿using UnityEngine;

namespace Chef
{
    // we use this scriptable object as replacement for enums
    // one of best practices to use scriptable objects
    [CreateAssetMenu(menuName = "Chef/VegetableType")]
    public class VegetableType : ScriptableObject { }
}