using System;
using UnityEngine;

namespace Chef
{
    public class VegetableBasket : MonoBehaviour
    {
        public Vegetable vegetablePrefab;
        public Vegetable Produce()
        {
            return Instantiate(vegetablePrefab);
        }
    }
}