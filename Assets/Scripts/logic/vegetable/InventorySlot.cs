using DG.Tweening;
using UnityEngine;

namespace Chef
{
    public class InventorySlot : MonoBehaviour
    {
        private const float inventoryScale = 0.8f;
        public Vegetable vegetable { get; private set; }
        public Salad salad { get; private set; }
        public bool filled { get { return (vegetable != null || salad != null); } }
        private void Awake()
        {
            vegetable = null;
            salad = null;
        }
        public void Fill(Vegetable vegetable)
        {
            vegetable.transform.SetParent(transform);
            vegetable.transform.position = transform.position;
            vegetable.transform.localScale = new Vector3(inventoryScale, inventoryScale, inventoryScale);
            this.vegetable = vegetable;
        }

        public void Fill(Salad salad)
        {
            salad.transform.SetParent(transform);
            salad.transform.position = transform.position;
            // salad.transform.localScale = transform.localScale;
            this.salad = salad;
        }
        public void Clean()
        {
            vegetable = null;
            salad = null;
        }
    }
}