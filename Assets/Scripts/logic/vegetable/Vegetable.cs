using UnityEngine;

namespace Chef
{
    public class Vegetable : MonoBehaviour
    {
        public VegetableType type;
        public Vegetable choppedPrefab;
    }
}