using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;

namespace Chef
{
    public class ProgressBar : MonoBehaviour
    {
        [SerializeField] Transform progressBar = null;
        public UnityEvent OnComplete = new UnityEvent();
        private Vector3 startScale;
        private Tweener tween;

        private void Awake()
        {
            startScale = progressBar.transform.localScale;
        }
        private void OnEnable()
        {
            progressBar.transform.localScale = startScale;
        }
        public void Play(float duration)
        {
            progressBar.transform.localScale = Vector3.one;
            tween = progressBar.DOScaleX(0, duration);
            tween.SetEase(Ease.Linear);
            tween.onComplete += Complete;
        }
        private void Complete()
        {
            if (OnComplete != null) OnComplete.Invoke();
        }

        internal void SetSpeed(float newSpeed)
        {
            tween.timeScale = newSpeed;
        }
    }
}