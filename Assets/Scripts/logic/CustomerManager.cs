using System.Collections.Generic;
using UnityEngine;

namespace Chef
{
    public class CustomerManager : MonoBehaviour
    {
        private const float orderVegetableScale = 0.3f;
        private const float SaladYOffset = -0.6f;

        // a database containing all possible chopped vegetables
        [SerializeField] ChoppedVegetableDatabase vegetableDatabase = null;
        // minimum and maximum amount of vegetables in customers salad
        [SerializeField] int minVegetableForSalad = 1;
        [SerializeField] int maxVegetableForSalad = 4;
        [SerializeField] List<Transform> customerSlots = new List<Transform>();
        // all prefabs that represent customers. this can be replaced with beautifful customers with animations
        [SerializeField] List<Customer> customerPrefabs = new List<Customer>();
        private static List<Customer> currentCustomers = new List<Customer>();
        private void Awake()
        {
            //first we fill all customer slots with new customers:
            for (int i = 0; i < customerSlots.Count; i++)
            {
                GenerateACustomer(i);
            }
        }

        private void GenerateACustomer(int i)
        {
            // Create a new customer from prefabs and assign to a slot
            var customer = Instantiate(customerPrefabs[Random.Range(0, customerPrefabs.Count)]);
            currentCustomers.Add(customer);
            customer.transform.SetParent(customerSlots[i], false);
            // create a new salad and initilize customer with that salad
            var salad = new GameObject("order-salad").AddComponent<Salad>();
            salad.transform.SetParent(customer.transform, false);
            salad.transform.Translate(0, SaladYOffset, 0);
            var saladVegetableCount = Random.Range(minVegetableForSalad, maxVegetableForSalad);
            for (int j = 0; j < saladVegetableCount; j++)
            {
                Vegetable vegetable = Instantiate(vegetableDatabase.vegetables[Random.Range(0, vegetableDatabase.vegetables.Count)]);
                vegetable.transform.position = salad.transform.position;
                vegetable.transform.localScale = new Vector3(orderVegetableScale, orderVegetableScale, orderVegetableScale);
                salad.AddVegetable(vegetable);
            }
            customer.Init(salad, this);
        }

        // This method is called by customer when he wants to go and need to be replaced
        public void OrderSuccess(Customer customer)
        {
            var index = currentCustomers.IndexOf(customer);
            currentCustomers.Remove(customer);
            GenerateACustomer(index);
        }
    }
}