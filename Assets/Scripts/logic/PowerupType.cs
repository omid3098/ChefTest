using UnityEngine;

namespace Chef
{
    public enum PowerupType
    {
        speed,
        time,
        score
    }
}