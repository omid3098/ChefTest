using System.Collections.Generic;
using UnityEngine;
namespace Chef
{
    /// <summary>
    /// Only one controller checks for all inputs
    /// </summary>
    public class InputController : MonoBehaviour
    {
        [SerializeField] List<PlayerInput> inputs = new List<PlayerInput>();
        void Update()
        {
            foreach (var input in inputs)
            {
                // check key press for each input
                if (Input.GetKey(input.up)) { input.vertical = 1; }
                if (Input.GetKey(input.down)) input.vertical = -1;
                if (Input.GetKey(input.left)) input.horizontal = -1;
                if (Input.GetKey(input.right)) input.horizontal = 1;
                if (Input.GetKeyDown(input.use)) { if (input.OnUsePressed != null) input.OnUsePressed.Invoke(); }

                // update values for ley release
                if (Input.GetKeyUp(input.up)) input.vertical = 0;
                if (Input.GetKeyUp(input.down)) input.vertical = 0;
                if (Input.GetKeyUp(input.left)) input.horizontal = 0;
                if (Input.GetKeyUp(input.right)) input.horizontal = 0;
            }
        }
    }
}