using UnityEngine;
using UnityEngine.Events;

namespace Chef
{
    [CreateAssetMenu(menuName = "Chef/PlayerInput")]
    public class PlayerInput : ScriptableObject
    {
        public float horizontal { get; set; }
        public float vertical { get; set; }
        public UnityEvent OnUsePressed;
        public KeyCode left;
        public KeyCode right;
        public KeyCode up;
        public KeyCode down;
        public KeyCode use;
    }
}