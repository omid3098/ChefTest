﻿using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Chef
{
    public class PlayerInteract : MonoBehaviour
    {
        private const string BasketTag = "basket";
        private const string ChoppingBoardTag = "chopping-board";
        private const string PlateTag = "plate";
        private const string TrashTag = "trash";
        private const string CustomerTag = "customer";
        [SerializeField] Collider2D _collider2D = null;
        [SerializeField] PlayerInput input = null;
        [SerializeField] List<InventorySlot> inventorySlots = new List<InventorySlot>();
        [SerializeField] BoolVariable waiting = null;
        [SerializeField] IntVariable score = null;
        static Vector3 otherColliderScale = new Vector3(0.01f, 0.01f, 0.01f);
        Vector3 otherColliderStartScale = Vector3.zero;
        private VegetableBasket currentBasket;
        private ChoppingBoard currentChoppingBoard;
        private Plate currentPlate;
        private Trash currentTrash;
        private Customer currentCustomer;
        private List<string> customersDeliveredWrong = new List<string>();
        private void Awake()
        {
            if (_collider2D == null) _collider2D = GetComponent<CircleCollider2D>();
        }
        private void OnEnable()
        {
            input.OnUsePressed.AddListener(OnUse);
            Customer.OnTimeOut.AddListener(OnCustomerTimeOut);
        }

        private void OnDisable()
        {
            input.OnUsePressed.RemoveListener(OnUse);
            Customer.OnTimeOut.RemoveListener(OnCustomerTimeOut);
        }

        /// <summary>
        /// Angry customer: The waiting time for the customer decreases faster. If he does not get
        // fed correctly within that time, the minus points are doubled and given solely to the
        // player that delivered the wrong meal. If both players delivered incorrectly, both are
        // penalized.
        /// </summary>
        /// <param name="customerID"></param>
        private void OnCustomerTimeOut(string customerID)
        {
            Debug.Log("Time is up for custome " + customerID);
            bool penalty = false;
            foreach (var id in customersDeliveredWrong)
            {
                if (id == customerID) penalty = true;
            }
            if (penalty)
            {
                score.value -= Settings.TimesUpPusishmentScore * 2;
            }
            else
            {
                score.value -= Settings.TimesUpPusishmentScore;
            }
        }

        private void OnUse()
        {
            if (!waiting.value)
            {
                // if we are near baskets, pick on of their products
                if (currentBasket != null)
                {
                    // find an empty slot and fill with basket product
                    var slot = GetEmptySlot();
                    if (slot != null) slot.Fill(currentBasket.Produce());
                }
                // if we are near chopping board, first chech if there are any products in chopping board or not?
                // if the chopping board has product, check in it is busy makeing or not.
                // if is not busy, pick product from chopping board into inventory
                // if chopping board is busy, wait.
                // if chopping board has no product and is not busy, put the product into it and make it busy
                else if (currentChoppingBoard != null)
                {
                    if (!currentChoppingBoard.busy)
                    {
                        // if there is veg in our inventory, put it into chopping board
                        var veg = PickVegetableFromInventory();
                        if (veg != null)
                        {
                            waiting.value = true;
                            currentChoppingBoard.Chop(veg);
                            currentChoppingBoard.OnFinish += StopWaiting;
                        }
                        // if no veg remains in inventory, check if we have salad, pick it
                        else
                        {
                            currentChoppingBoard.OnFinish -= StopWaiting;
                            if (!currentChoppingBoard.salad.isEmpty)
                            {
                                if (NoVegetableInInventory())
                                {
                                    var slot = GetEmptySlot();
                                    if (slot != null)
                                    {
                                        Salad salad = currentChoppingBoard.GetSalad();
                                        Debug.Log("Salad produced");
                                        slot.Fill(salad);
                                        Debug.Log("Slot filled with salad");
                                    }
                                }
                            }
                        }
                    }
                }
                else if (currentPlate != null)
                {
                    if (currentPlate.currentVegetable == null)
                    {
                        // pick a vegerable from inventory and put it in plate
                        var veg = PickVegetableFromInventory();
                        if (veg != null) currentPlate.Fill(veg);
                        Debug.Log("put in the plate");
                    }
                    else
                    {
                        // find an empty slot and fill with plate vegetable
                        var slot = GetEmptySlot();
                        if (slot != null)
                        {
                            slot.Fill(currentPlate.Get());
                            currentPlate.Clean();
                            Debug.Log("pick from the plate");
                        }
                    }
                }
                else if (currentTrash != null)
                {
                    // put the salad from inventory into trash
                    Salad salad = PickSaladFromInventory();
                    if (salad != null) currentTrash.Recycle(salad);
                }
                else if (currentCustomer != null)
                {
                    // compare current salad in Inventory with the current customer order
                    Salad salad = PickSaladFromInventory();
                    if (salad != null)
                    {
                        var result = currentCustomer.ReceiveOrder(salad);
                        // The score count increases when the player gives the right combination to a customer
                        if (result.delivetStatus) score.value += Settings.CorrectScore;
                        else customersDeliveredWrong.Add(result.customerID);
                    }
                }
            }
        }

        private void StopWaiting()
        {
            waiting.value = false;
        }

        private bool NoVegetableInInventory()
        {
            foreach (var slot in inventorySlots)
            {
                if (slot.filled) return false;
            }
            return true;
        }

        private Vegetable PickVegetableFromInventory()
        {
            InventorySlot filledSlot = null;
            foreach (var slot in inventorySlots)
            {
                if (slot.vegetable != null && slot.salad == null)
                {
                    filledSlot = slot;
                    break;
                }
            }
            if (filledSlot != null)
            {
                Vegetable vegetable = filledSlot.vegetable;
                filledSlot.Clean();
                return vegetable;
            }
            else return null;
        }
        private Salad PickSaladFromInventory()
        {
            InventorySlot filledSlot = null;
            foreach (var slot in inventorySlots)
            {
                if (slot.vegetable == null && slot.salad != null)
                {
                    filledSlot = slot;
                    break;
                }
            }
            if (filledSlot != null)
            {
                Salad salad = filledSlot.salad;
                filledSlot.Clean();
                return salad;
            }
            else return null;
        }

        private InventorySlot GetEmptySlot()
        {
            InventorySlot emptySlot = null;
            foreach (var slot in inventorySlots)
            {
                if (!slot.filled)
                {
                    emptySlot = slot;
                    break;
                }
            }
            return emptySlot;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.tag == BasketTag)
            {
                PopInteractable(other);
                currentBasket = other.GetComponent<VegetableBasket>();
            }
            else if (other.tag == ChoppingBoardTag)
            {
                PopInteractable(other);
                currentChoppingBoard = other.GetComponent<ChoppingBoard>();
            }
            else if (other.tag == PlateTag)
            {
                PopInteractable(other);
                currentPlate = other.GetComponent<Plate>();
            }
            else if (other.tag == TrashTag)
            {
                PopInteractable(other);
                currentTrash = other.GetComponent<Trash>();
            }
            else if (other.tag == CustomerTag)
            {
                PopInteractable(other, 10f);
                currentCustomer = other.GetComponentInChildren<Customer>();
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.tag == BasketTag)
            {
                ScaleDownOther(other);
                currentBasket = null;
            }
            else if (other.tag == ChoppingBoardTag)
            {
                ScaleDownOther(other);
                currentChoppingBoard = null;
            }
            else if (other.tag == PlateTag)
            {
                ScaleDownOther(other);
                currentPlate = null;
            }
            else if (other.tag == TrashTag)
            {
                ScaleDownOther(other);
                currentTrash = null;
            }
            else if (other.tag == CustomerTag)
            {
                ScaleDownOther(other);
                currentCustomer = null;
            }
        }

        private void ScaleDownOther(Collider2D other)
        {
            var tween = other.transform.DOScale(otherColliderStartScale, 0.4f);
            tween.SetEase(Ease.OutElastic);
        }

        private void PopInteractable(Collider2D other, float scaleFactor = 1)
        {
            otherColliderStartScale = other.transform.localScale;
            var tween = other.transform.DOScale(otherColliderScale * scaleFactor, 0.4f);
            tween.SetEase(Ease.OutElastic);
            tween.SetRelative(true);
        }
    }
}