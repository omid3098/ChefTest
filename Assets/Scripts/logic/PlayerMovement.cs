﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Assertions;

namespace Chef
{
    /// <summary>
    /// Input values will be calculated in InputController and results will be updated in each input object
    /// So we only use them for movement
    /// </summary>
    public class PlayerMovement : MonoBehaviour
    {
        [SerializeField] PlayerInput input = null;
        public float speed { get; private set; }
        [SerializeField] Rigidbody2D rigidBody;
        [SerializeField] BoolVariable waiting = null;

        private void Awake()
        {
            if (rigidBody == null) rigidBody = GetComponent<Rigidbody2D>();
            Assert.IsNotNull(rigidBody, "Please make sure to have rigidbody component on player to move");
            speed = Settings.PlayerSpeed;
        }
        private void FixedUpdate()
        {
            if (waiting.value)
            {
                if (rigidBody.velocity != Vector2.zero) rigidBody.velocity = Vector2.zero;
                return;
            }
            if (input.horizontal == 0 && input.vertical == 0) rigidBody.velocity = Vector2.zero;
            else
            {
                rigidBody.velocity = Vector2.zero;
                rigidBody.AddForce(new Vector3(input.horizontal * speed * 100, input.vertical * speed * 100));
            }
        }
    }
}