﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
namespace Chef
{
    public class CameraController : MonoBehaviour
    {
        // aka player 1. but we may need this script somewhere without players so we will use 
        [SerializeField] Transform point1 = null;
        // aka player 2
        [SerializeField] Transform point2 = null;
        // minimum camera zoom
        [SerializeField] float minZoom = 5;
        // maximun camera zoom
        [SerializeField] float maxZoom = 3;
        // to increase performance update will run each frame
        [SerializeField] int updateInterval = 3;
        // the time that camera need to reach its new position and zoom factor
        [SerializeField] float tweenDuration = 0.5f;
        // holds the main scene camera
        [SerializeField] Camera mainCamera;

        private void Awake()
        {
            if (mainCamera == null)
            {
                mainCamera = Camera.main;
                Debug.LogWarning("Please assign a camera in Main Camera field to keep performece. finding camera in code looses that. this message is harmless.");
            }
        }
        private void Update()
        {
            if (Time.frameCount % updateInterval == 0)
            {
                // we need points distance to calculate zoom factor
                var distance = Vector3.Distance(point1.position, point2.position);
                distance = Mathf.Clamp(distance, maxZoom, minZoom);
                DOTween.To(() => mainCamera.orthographicSize, (x) => mainCamera.orthographicSize = x, distance, tweenDuration);

                // we need center point to calculate camera best position
                Vector3 center = (point1.position + point2.position) / 2;
                // we use tweening to smoothly move camera to a new position
                mainCamera.transform.DOMove(new Vector3(center.x, center.y, mainCamera.transform.position.z), tweenDuration);
            }
        }
    }
}