using DG.Tweening;
using UnityEngine;

namespace Chef
{
    public class ChoppingBoard : MonoBehaviour
    {
        public Vegetable currentVegetable { get; private set; }

        private const float VegetableMoveDurarion = 0.4f;
        private const float ChoppedScaleupDuration = 0.3f;
        [SerializeField] ProgressBar progressBar = null;
        [SerializeField] Salad _salad;
        public delegate void ProductDelegete();
        public event ProductDelegete OnFinish;
        public Salad salad { get { return _salad; } private set { _salad = value; } }
        public bool busy { get; private set; }
        private void Awake()
        {
            progressBar.gameObject.SetActive(false);
        }
        public void Chop(Vegetable vegetable)
        {
            vegetable.transform.SetParent(salad.transform);
            var tween = vegetable.transform.DOMove(salad.transform.position, VegetableMoveDurarion);
            tween.SetEase(Ease.OutQuart);
            progressBar.gameObject.SetActive(true);
            busy = true;
            currentVegetable = vegetable;
            progressBar.Play(Settings.ChoppingBoardWaitTime);
            progressBar.OnComplete.AddListener(Finished);
        }

        private void Finished()
        {
            progressBar.OnComplete.RemoveListener(Finished);
            progressBar.gameObject.SetActive(false);
            var choppedResult = Instantiate(currentVegetable.choppedPrefab);
            choppedResult.transform.localScale = Vector3.zero;
            choppedResult.transform.SetParent(salad.transform);
            choppedResult.transform.position = salad.transform.position;
            var tween = choppedResult.transform.DOScale(currentVegetable.transform.localScale, ChoppedScaleupDuration);
            tween.SetEase(Ease.OutElastic);
            tween.onComplete += () =>
            {
                busy = false;
                if (OnFinish != null) OnFinish.Invoke();
            };
            salad.AddVegetable(choppedResult);
            Destroy(currentVegetable.gameObject);
            currentVegetable = null;
        }

        internal Salad GetSalad()
        {
            return salad.Produce();
        }
    }
}