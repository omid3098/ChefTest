using DG.Tweening;
using UnityEngine;

namespace Chef
{
    public static class TransitionHelper
    {
        public static void MoveVegetable(Transform vegetable, Vector3 targetPos)
        {
            var tween = vegetable.DOMove(targetPos, 0.4f);
            tween.SetEase(Ease.OutQuart);
        }
    }
}