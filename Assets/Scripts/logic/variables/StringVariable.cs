using UnityEngine;

namespace Chef
{
    [CreateAssetMenu(menuName = "Variables/string")]
    public class StringVariable : GenericVariable<string> { }
}