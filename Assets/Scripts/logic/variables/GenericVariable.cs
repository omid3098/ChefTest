using UnityEngine;

namespace Chef
{
    public class GenericVariable<TEnum> : ScriptableObject
    {
        public TEnum value;
        private void OnEnable()
        {
            value = default(TEnum);
        }
    }
}