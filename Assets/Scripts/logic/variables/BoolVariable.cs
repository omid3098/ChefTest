using UnityEngine;

namespace Chef
{

    [CreateAssetMenu(menuName = "Variables/bool")]
    public class BoolVariable : GenericVariable<bool> { }
}