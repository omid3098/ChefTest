using UnityEngine;

namespace Chef
{
    [CreateAssetMenu(menuName = "Variables/int")]
    public class IntVariable : GenericVariable<int> { }
}