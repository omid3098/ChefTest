﻿using UnityEngine;
using UnityEngine.UI;

namespace Chef
{
    public class PlayerStatsUi : MonoBehaviour
    {
        [SerializeField] Text scoreText;
        [SerializeField] Text timeText;
        [SerializeField] IntVariable score;
        [SerializeField] IntVariable time;
        private float clock;
        private void Awake()
        {
            clock = Settings.PlayersStartTimer;
        }
        private void Update()
        {
            clock -= Time.deltaTime;
            if (clock < 0) clock = 0;
            else
            {
                time.value = Mathf.FloorToInt(clock);
                scoreText.text = "Score: " + score.value;
                timeText.text = "Time: " + time.value;
            }
        }
    }
}