using UnityEngine;

namespace Chef
{
    public static class Settings
    {
        public const float PlayerSpeed = 2f;
        public const float ChoppingBoardWaitTime = 2f;
        public const float OrderWaitTimePerVegetable = 20f;
        public const int CorrectScore = 100;
        public const int TimesUpPusishmentScore = 100;
        public const float AngryCustomerProgressSpeed = 2f;

        // players start time in seconds;
        public const int PlayersStartTimer = 100;
    }
}