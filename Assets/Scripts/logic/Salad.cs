using System.Collections.Generic;
using UnityEngine;

namespace Chef
{
    public class Salad : MonoBehaviour
    {
        private const float xOffset = 0.2f;
        private List<Vegetable> _vegetables = new List<Vegetable>();
        public int vegetableCount { get { return _vegetables.Count; } }
        public List<Vegetable> vegetables { get { return _vegetables; } }
        public void AddVegetable(Vegetable vegetable)
        {
            vegetable.transform.SetParent(transform);
            vegetable.transform.position = transform.position + new Vector3(_vegetables.Count * xOffset, 0, 0);
            _vegetables.Add(vegetable);
        }
        public bool isEmpty { get { return _vegetables.Count == 0; } }

        internal Salad Produce()
        {
            var newSalad = new GameObject("salad").AddComponent<Salad>();
            newSalad.transform.position = transform.position;
            for (int i = 0; i < _vegetables.Count; i++)
            {
                Vegetable veg = _vegetables[i];
                veg.transform.SetParent(newSalad.transform);
                veg.transform.position = newSalad.transform.position + new Vector3(i * xOffset, 0, 0);
                newSalad.AddVegetable(veg);
            }
            _vegetables.Clear();
            return newSalad;
        }
    }
}