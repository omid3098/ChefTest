# ChefTest

## How to play:
player blue moves with wasd keys and interact with items using left ctrl
player red moves with arrow keys and interact with items using right ctrl
you can change these values in "Assets/Static-Assets/player-input/" scriptable objects

## Implemented features:
- Static top-down view on the level or camera that pans in/out depending on distance between players in the level.
- Couch Co-Op”: Two players play on one machine using different keys to control two characters
- Implementation of different vegetables
- Vegetables can be picked up from both sides of the room (red and blue tables)
- A picked vegetable can be placed on the “black” chopping board. It takes some time to chop the vegetable. Meanwhile, the player cannot move.
- A Player can carry two vegetables at a time. The item, which was picked up first, will also be placed on the chopping board first. There should be an indicator for items that are currently picked up.
- Once the item is chopped, other items can be placed on the same chopping board to create different combinations.
- A combination can be picked up from the chopping board again and placed on a table for a customer. The player can also throw the salad into the trashcan.
- The player can place one vegetable on the plate next to the chopping board to be able to pick up a combination from the chopping board.
- The score count increases when the player gives the right combination to a customer
- A customer waits for a defined period before leaving dissatisfied. The period depends on the amount of ingredients.
- Delivering in time adds points to the player’s score count. If the time runs out, the customer will leave which will result in minus points for both players.
- Incorrect combinations will make the customer angry.
- Angry customer: The waiting time for the customer decreases faster. If he does not get fed correctly within that time, the minus points are doubled and given solely to the player that delivered the wrong meal. If both players delivered incorrectly, both are
penalized.

## Not Implemented features:
### Pickups: 
- needs a pickup manager in the scene, that produces randomly from pickup prefabs. pickup class needs a pickupType and each prefab has a unity pickupType (speed,time,score) with a collider2d, player can autocollect pickups and after comparing pickupType, changes the equivalent object. for speed, we need a speed multipler in PlayerMovement class to have a default value of 1 and a timer to reset speed back to 1.
### Game end:
- Just a checker for both players timers and if both are equal to 0, pause the game and activate a game end Ui.
### HighScore:
- Save player scores as their score gets higher, if they new score is higer than their previous one, set a new highscore. we also need a class that holds top 10 highscores in a list, a new highscore will insterted at the top of the list and everything can be saved as a serialized data in disk. easy save and easy load.
### Reset the game:
A button to reload the current scene!
